﻿using System;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Encripta
{
    public class EncryptClass
    {
        #region "Parametros de la Clase"

        public string DescripcionError { get; set; }
        public int CodigoError { get; set; }

        public bool pEnAccion { get; set; }

        private string ppKeyEncriptar = "123456AAFA123456AAFA123456AAFA123456AAFA123456AA";

        public string pKeyEncriptar
        {
            get { return ppKeyEncriptar; }
            set { ppKeyEncriptar = value; }
        }

        private string ppKeyEncriptar32 = "123456AA123456AA";
        public string pKeyEncriptar32
        {
            get { return ppKeyEncriptar32; }
            set { ppKeyEncriptar32 = value; }
        }

        private string ppIVEncriptar = "123456AAFA123456AAFA123456AAFA123456AAFA123456AA123456AAFA123456AAFA123456AAFA123456AAFA123456AA123456AAFA123456AAFA123456AAFA12";
        public string pIVEncriptar
        {
            get { return ppIVEncriptar; }
            set { ppIVEncriptar = value; }
        }


        private string ppIVEncriptar32 = "123456AAFA123456AAFA123456AAFA12";
        public string pIVEncriptar32
        {
            get { return ppIVEncriptar32; }
            set { ppIVEncriptar32 = value; }
        }

        private bool ppConectadoCentral = true;
        public bool pConectadoCentral
        {
            get { return ppConectadoCentral; }
            set { ppConectadoCentral = value; }
        }

        private int ppIdPeriodo = -1;
        public int pIdPeriodo
        {
            get { return ppIdPeriodo; }
            set { ppIdPeriodo = value; }
        }

        private object ppUsuarioActual = null;
        public object pUsuarioActual
        {
            get { return ppUsuarioActual; }
            set { ppUsuarioActual = value; }
        }

        private int ppMonedaLocal = -1;
        public int pMonedaLocal
        {
            get { return ppMonedaLocal; }
            set { ppMonedaLocal = value; }
        }
        private int ppMonedaSistema = -1;
        public int pMonedaSistema
        {
            get { return ppMonedaSistema; }
            set { ppMonedaSistema = value; }
        }

        #endregion

        public string Encripta_General(string strToEncrypt)
        {
            string Key = ppKeyEncriptar;
            string IV = ppIVEncriptar;

            MemoryStream output = new MemoryStream();
            byte[] byteData = new UnicodeEncoding().GetBytes(strToEncrypt);

            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            byte[] desKey = new byte[Key.Length / 2];

            for (int i = 0; i < Key.Length; i += 2)
            {
                desKey[i / 2] = byte.Parse(Key.Substring(i, 2), NumberStyles.HexNumber);
            }

            byte[] desIV = new byte[IV.Length / 2];
            for (int i = 0; i < IV.Length; i += 2)
            {
                desIV[i / 2] = byte.Parse(IV.Substring(i, 2), NumberStyles.HexNumber);
            }

            CryptoStream crypt = new CryptoStream(output, des.CreateEncryptor(desKey, desIV), CryptoStreamMode.Write);
            crypt.Write(byteData, 0, byteData.Length);
            crypt.Close(); output.Close();


            byte[] bin = output.ToArray();
            /*
            string strData = "";
            foreach (byte b in bin)
            {
                strData += b.ToString("x2");
            }
            */
            return (Convert.ToBase64String(bin)); ;

        }
        public string Desencripta_General(string data)
        {
            string Key = ppKeyEncriptar;
            string IV = ppIVEncriptar;

            MemoryStream output = new MemoryStream();
            //ata = Convert.ToBase64String(data);
            byte[] byteData = Convert.FromBase64String(data);//new byte[data.Length / 2];
            /*
            for (int i = 0; i < data.Length; i+=2)
            {
                byteData[i/2] = byte.Parse(data.Substring(i, 2), NumberStyles.Any);
            }
             */

            byte[] desKey = new byte[Key.Length / 2];
            for (int i = 0; i < Key.Length; i += 2)
            {
                desKey[i / 2] = byte.Parse(Key.Substring(i, 2), NumberStyles.HexNumber);
            }

            byte[] desIV = new byte[IV.Length / 2];
            for (int i = 0; i < IV.Length; i += 2)
            {
                desIV[i / 2] = byte.Parse(IV.Substring(i, 2), NumberStyles.HexNumber);
            }

            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            CryptoStream crypt = new CryptoStream(output, des.CreateDecryptor(desKey, desIV), CryptoStreamMode.Write);
            crypt.Write(byteData, 0, byteData.Length);
            crypt.Close(); output.Close();

            return new UnicodeEncoding().GetString(output.ToArray());
        }
    }
}
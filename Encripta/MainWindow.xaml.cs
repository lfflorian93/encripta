﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Encripta
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        EncryptClass encrypt = new EncryptClass();

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (txtRecibe.IsSelectionActive)
                {
                    txtResultado.Text = encrypt.Encripta_General(txtRecibe.Text);
                }
            }
            catch (Exception)
            {
                
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(txtResultado.Text);
        }

        private void txtResultado_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {

                if (txtResultado.IsSelectionActive)
                {
                    txtRecibe.Text = encrypt.Desencripta_General(txtResultado.Text);
                }
            }
            catch (Exception)
            {
                txtRecibe.Text = "Texto Erroneo";
            }
        }
    }
}
